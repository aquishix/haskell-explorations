import Control.Monad (when)
import Data.Char (chr)
main = do
 loop 10
  where loop n = when (n > 0)
                      (do putChar (chr n)
                          loop (n - 1))
-- putStrLn "Dammit."



