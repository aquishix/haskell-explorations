import System.IO

repeatNTimes 0 _ = return ()
repeatNTimes n action = 
 do
  action
  repeatNTimes (n-1) action


-- putStrLn ""

  

import Control.Monad (when)
main = loop 10
  where loop n = when (n > 0)
                      (do print n
                          loop (n `div` 2))


