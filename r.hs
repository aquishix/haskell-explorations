-- r :: [Char] -> [Char]
r st = [ c | c <- st, c `elem` ['A'..'Z']]

factorial :: Integer -> Integer
factorial n = product [1..n]

factorial2 :: Int -> Int
factorial2 n = product [1..n]

factorial3 n = product [1..n]

circumference :: Float -> Float
circumference r = 2 * pi * r

circumference2 :: Double -> Double
circumference2 r = 2 * pi * r

circumference3 r = 2 * pi * r

lucky :: (Integral a) => a -> String
lucky 7 = "LUCKY NUMBER SEVEN!@"
lucky x = "Sorry, you're out of lucky, son."

sayMe :: (Integral n) => n -> String
sayMe 1 = "Uno!"
sayMe 2 = "Dos!"
sayMe 3 = "Tres!"
sayMe 4 = "Quatro!"
sayMe 5 = "Cinco!"
sayMe n = "No dentro de 1 y 5"

factorial4 :: (Integral a) => a -> a
factorial4 0 = 1
factorial4 n = n * factorial4 (n - 1)

first :: (a, b, c) -> a
first (x, _, _) = x

second :: (a, b, c) -> b
second (_, y, _) = y

third :: (a, b, c) -> c
third (_, _, z) = z

head' :: [a] -> a
head' [] = error "Can't call head on an empty list, dumbass!"
head' (x:_) = x

tell :: (Show a) => [a] -> String
tell [] = "The list is empty."
tell (x:[]) = "The list has one element: " ++ show x
tell (x:y:[]) = "The list has two elements: " ++ show x ++ " and " ++ show y
tell (x:y:_) = "This list has at least 3 elements.  The first two are: " ++ show x ++ " and " ++ show y

length' :: (Num b) => [a] -> b
length' [] = 0
length' (_:xs) = 1 + length' xs

sum' :: (Num a) => [a] -> a
sum' [] = 0
sum' (x:xs) = x + sum' xs

product' :: (Num a) => [a] -> a
product' [] = 1
product' (x:xs) = x * product' xs

capital :: String -> String
capital "" = "Empty string; whoops!"
capital all@(x:xs) = "The first letter of " ++ all ++ " is " ++ [x]

bmiTell :: (RealFloat a) => a -> String
bmiTell bmi
    | bmi <= 18.5 = "You're underweight, loser."
    | bmi <= 25.0 = "What's it like to be normal?"
    | bmi <= 30.0 = "You're disgusting.  Cut it out!"
    | otherwise   = "It may be too late for you.  Try not to get harpooned!"

bmiTell2 :: (RealFloat a) => a -> a -> String
bmiTell2 weight height
    | weight / height ^ 2 <= 18.5 = "You're underweight, loser."
    | weight / height ^ 2 <= 25.0 = "What's it like to be normal?"
    | weight / height ^ 2 <= 30.0 = "You're disgusting.  Cut it out!"
    | otherwise   = "It may be too late for you.  Try not to get harpooned!"

max' :: (Ord a) => a -> a -> a
max' a b
    | a > b     = a
    | otherwise = b

myCompare :: (Ord a) => a -> a -> Ordering
a `myCompare` b
    | a > b     = GT
    | a == b    = EQ
    | otherwise = LT

bmiTell3 :: (RealFloat a) => a -> a -> String
bmiTell3 weight height
    | bmi <= skinny = "You're underweight, loser."
    | bmi <= normal = "What's it like to be normal?"
    | bmi <= fat = "You're disgusting.  Cut it out!"
    | otherwise   = "It may be too late for you.  Try not to get harpooned!"
    where bmi = weight / height ^ 2
          skinny = 18.5
          normal = 25.0
          fat = 30.0

bmiTell4 :: (RealFloat a) => a -> a -> String
bmiTell4 weight height
    | bmi <= skinny = "You're underweight, loser."
    | bmi <= normal = "What's it like to be normal?"
    | bmi <= fat = "You're disgusting.  Cut it out!"
    | otherwise   = "It may be too late for you.  Try not to get harpooned!"
    where bmi = weight / height ^ 2
          (skinny, normal, fat) = (18.5, 25.0, 30.0)

initials :: String -> String -> String
initials firstname lastname = [f] ++ ". " ++ [l] ++ "."
    where (f:_) = firstname
          (l:_) = lastname

calcBmis :: (RealFloat a) => [(a, a)] -> [a]
calcBmis xs = [bmi w h | (w, h) <- xs]
    where bmi weight height = weight / height ^ 2

cylinder :: (RealFloat a) => a -> a -> a
cylinder r h = 
    let sideArea = 2 * pi * r * h
        topArea = pi * r  ^ 2
    in sideArea + 2 * topArea

calcBmis2 :: (RealFloat a) => [(a,a)] -> [a]
calcBmis2 xs = [bmi | (w, h) <- xs, let bmi = w / h ^2]

calcBmis3 :: (RealFloat a) => [(a,a)] -> [a]
calcBmis3 xs = [bmi | (w, h) <- xs, let bmi = w / h ^2, bmi >= 25.0]

head'' :: [a] -> a
head'' [] = error "Try again with a non-empty list..."
head'' (x:_) = x

head''' :: [a] -> a
head''' xs = case xs of [] -> error "Try again with a non-empty list..." 
                        (x:_) -> x


describeList :: [a] -> String
describeList xs = "The list " ++ case xs of [] -> "is empty."
                                            [x] -> "is a singleton."
                                            (x:[y]) -> "contains 2 elements."
                                            (x:y:[z]) -> "contains 3 elements."
                                            xs -> "...has at least 4."

describeList2 :: [a] -> String
describeList2 xs = "The list is " ++ what xs
    where what [] = "empty."
          what [x] = "a singleton list."
          what xs = "a longer list."

maximum' :: (Ord a) => [a] -> a
maximum' [] = error "maximum of empty list"
maximum' [x] = x
maximum' (x:xs)
    | x > maxTail = x
    | otherwise = maxTail
    where maxTail = maximum' xs
 
